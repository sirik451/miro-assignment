export const validUserData = {
    name: "goodName",
    email: `${Cypress._.random(0, 1e6)}@yuki.com`,
    password: "akutagawaIsagood_writer!",
    checkTermsAndConditionsCheckbox: true,
    checkSubscriptionCheckbox: true,
  };

  export const emptyNameUserData = {
    name: " ",
    email: "example@yuki.com",
    password: "akutagawaIsagood_writer!",
    checkTermsAndConditionsCheckbox: true,
    checkSubscriptionCheckbox: true,
  };

  export const emptyEmailUserData = {
    name: "goodName",
    email: " ",
    password: "akutagawaIsagood_writer!",
    checkTermsAndConditionsCheckbox: true,
    checkSubscriptionCheckbox: true,
  };

  export const incorrectFormatEmailUserData = {
    name: "goodName",
    email: "a.co",
    password: "akutagawaIsagood_writer!",
    checkTermsAndConditionsCheckbox: true,
    checkSubscriptionCheckbox: true,
  };

  export const existingEmailUserData = {
    name: "goodName",
    email: "a@b.co",
    password: "akutagawaIsagood_writer!",
    checkTermsAndConditionsCheckbox: true,
    checkSubscriptionCheckbox: true,
  };

  export const emptyPasswordUserData = {
    name: "goodName",
    email: "example@yuki.com",
    password: " ",
    checkTermsAndConditionsCheckbox: true,
    checkSubscriptionCheckbox: true,
  };

  export const shortPasswordUserData = {
    name: "goodName",
    email: "example@yuki.com",
    password: "shortpa",
    checkTermsAndConditionsCheckbox: true,
    checkSubscriptionCheckbox: true,
  };

  export const weakPasswordUserData = {
    name: "goodName",
    email: "example@yuki.com",
    password: "shortpas",
    checkTermsAndConditionsCheckbox: true,
    checkSubscriptionCheckbox: true,
  };

  export const sosoPasswordUserData = {
    name: "goodName",
    email: "example@yuki.com",
    password: "shortpas1",
    checkTermsAndConditionsCheckbox: true,
    checkSubscriptionCheckbox: true,
  };

  export const goodPasswordUserData = {
    name: "goodName",
    email: "example@yuki.com",
    password: "shortpas1kk",
    checkTermsAndConditionsCheckbox: true,
    checkSubscriptionCheckbox: true,
  };
  export const uncheckedTermsUserData = {
    name: "goodName",
    email: "example@yuki.com",
    password: "akutagawaIsagood_writer!",
    checkTermsAndConditionsCheckbox: false,
    checkSubscriptionCheckbox: true,
  };