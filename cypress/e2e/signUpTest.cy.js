import { signUpPage } from "../pages/signUpPage";
import * as userTestData from "../fixtures/userTestData";
import * as signUpPageConstants from "../fixtures/signUpPageConstants.json";
import { signUpIntercepts } from "../pages/signUpIntercepts";

describe("Sign-up page test cases", () => {
  beforeEach(() => {
    signUpPage.navigateToSignUpPage();
    signUpIntercepts();
  });

  it("make sure user gets errors when trying to signup without name", () => {
    signUpPage.fillSignUpForm(userTestData.validUserData);
    signUpPage.clickGetStartedNowBtn();
    cy.wait("@postCollect");
    signUpPage
      .getSignUpSuccessPageTitle()
      .should("have.text", signUpPageConstants.successPageTitle);
    signUpPage
      .getSignUpSuccessPageSubtitle()
      .should("include.text", userTestData.validUserData.email);
  });

  it("make sure user gets errors when trying to submit empty signup form", () => {
    signUpPage.clickGetStartedNowBtn();
    cy.wait("@postCollect");
    signUpPage
      .getNameError()
      .should("have.text", signUpPageConstants.missingNameError);
    signUpPage
      .getEmailError()
      .should("have.text", signUpPageConstants.missingEmailError);
    signUpPage
      .getPasswordError()
      .should("have.text", signUpPageConstants.missingPasswordError);
    signUpPage
      .getTermsAndConditionsError()
      .should("have.text", signUpPageConstants.uncheckedTermsError);
  });

  it("make sure user gets errors when trying to signup without name", () => {
    signUpPage.fillSignUpForm(userTestData.emptyNameUserData);
    signUpPage.clickGetStartedNowBtn();
    cy.wait("@postCollect");
    signUpPage
      .getNameError()
      .should("have.text", signUpPageConstants.missingNameError);
  });

  it("make sure user gets errors when trying to signup without email", () => {
    signUpPage.fillSignUpForm(userTestData.emptyEmailUserData);
    signUpPage.clickGetStartedNowBtn();
    cy.wait("@postCollect");
    signUpPage
      .getEmailError()
      .should("have.text", signUpPageConstants.missingEmailError);
  });

  it("make sure user gets errors when trying to signup with incorrect format email", () => {
    signUpPage.fillSignUpForm(userTestData.incorrectFormatEmailUserData);
    signUpPage.clickGetStartedNowBtn();
    cy.wait("@postCollect");
    signUpPage
      .getEmailError()
      .should("have.text", signUpPageConstants.invalidEmailError);
  });

  it("make sure user gets errors when trying to signup without password", () => {
    signUpPage.fillSignUpForm(userTestData.emptyPasswordUserData);
    signUpPage.clickGetStartedNowBtn();
    cy.wait("@postCollect");
    signUpPage
      .getPasswordError()
      .should("have.text", signUpPageConstants.missingPasswordError);
  });

  it("make sure user gets errors when trying to signup with short password", () => {
    signUpPage.fillSignUpForm(userTestData.shortPasswordUserData);
    cy.wait("@postCollect");
    signUpPage
      .getShortPasswordError()
      .should("have.text", signUpPageConstants.shortPasswordError);
  });

  it("make sure user gets errors when trying to signup with weak password", () => {
    signUpPage.fillSignUpForm(userTestData.weakPasswordUserData);
    cy.wait("@postCollect");
    signUpPage
      .getWeakPasswordError()
      .should("have.text", signUpPageConstants.weakPasswordMessage);
  });

  it("make sure user gets errors when trying to signup with so-so password", () => {
    signUpPage.fillSignUpForm(userTestData.sosoPasswordUserData);
    cy.wait("@postCollect");
    signUpPage
      .getSoSoPasswordError()
      .should("have.text", signUpPageConstants.sosoPasswordMessage);
  });

  it("make sure user gets errors when trying to signup with good password", () => {
    signUpPage.fillSignUpForm(userTestData.goodPasswordUserData);
    cy.wait("@postCollect");
    signUpPage
      .getGoodPasswordMessage()
      .should("have.text", signUpPageConstants.goodPasswordMessage);
  });

  it("make sure user gets errors when trying to signup with existing password", () => {
    signUpPage.fillSignUpForm(userTestData.existingEmailUserData);
    signUpPage.clickGetStartedNowBtn();
    cy.wait("@postCollect");
    signUpPage
      .getEmailError()
      .should("have.text", signUpPageConstants.existingEmailError);
  });

  it("make sure user gets errors when trying to signup without agreeing with terms and conditions", () => {
    signUpPage.fillSignUpForm(userTestData.uncheckedTermsUserData);
    signUpPage.clickGetStartedNowBtn();
    cy.wait("@postCollect");
    signUpPage
      .getTermsAndConditionsError()
      .should("have.text", signUpPageConstants.uncheckedTermsError);
  });

  it("make sure user redirected to Terms of Service page", () => {
    signUpPage.clickTermLink();
    cy.location("pathname").should("equal", "/legal/terms-of-service/");
  });

  it("make sure user redirected to Privacy Policy page", () => {
    signUpPage.clickPrivacyPolicyLink();
    cy.location("pathname").should("equal", "/legal/privacy-policy/");
  });

  it("make sure Sign up with Google button opens a popup", () => {
    signUpPage.clickSignUpWithGoogleBtn();
    signUpPage
      .getSocialSitesLoginPageTitle()
      .should("have.text", "Review the Terms to sign up");
  });

  it("make sure Sign up with Slack button opens a popup", () => {
    signUpPage.clickSlackBtn();
    signUpPage
      .getSocialSitesLoginPageTitle()
      .should("have.text", "Review the Terms to sign up");
  });

  it("make sure Sign up with Microsoft Office button opens a popup", () => {
    signUpPage.clickOfficeBtn();
    signUpPage
      .getSocialSitesLoginPageTitle()
      .should("have.text", "Review the Terms to sign up");
  });

  it("make sure Sign up with Apple authentication button opens a popup", () => {
    signUpPage.clickAppleBtn();
    signUpPage
      .getSocialSitesLoginPageTitle()
      .should("have.text", "Review the Terms to sign up");
  });

  it("make sure Sign up with Facebook  button opens a popup", () => {
    signUpPage.clickFacebookBtn();
    signUpPage
      .getSocialSitesLoginPageTitle()
      .should("have.text", "Review the Terms to sign up");
  });
});
