export const name = () => cy.get("[data-testid='mr-form-signup-name-1']");
export const email = () => cy.get("[data-testid='mr-form-signup-email-1']");
export const password = () =>
  cy.get("[data-testid='mr-form-signup-password-1']");
export const termsAndConditions = () =>
  cy.get(
    "[data-testid='mr-form-signup-terms-1'] > .mr-checkbox-1__wrap > .mr-checkbox-1__check");
export const subscriptionCheckbox = () =>
  cy.get(
    "[data-testid='mr-form-signup-subscribe-1'] > .mr-checkbox-1__wrap > .mr-checkbox-1__check");
export const submitBtn = () =>
  cy.get("[data-testid='mr-form-signup-btn-start-1']");
export const nameError = () => cy.get("#nameError");
export const emailError = () => cy.get('#emailError');
export const passwordError = () => cy.get("[data-testid='please-enter-your-password-1']");
export const shortPassword = () => cy.get("[data-text-default='Please use 8+ characters for secure password.']");
export const weakPassword = () => cy.get("[data-text-weak='Weak password']");
export const sosoPassword = () => cy.get("[data-text-soso='So-so password']");
export const goodPassword = () => cy.get("[data-text-good='Good password']");
export const termsError = () => cy.get("#termsError");
export const signUpSuccessPageTitle = () => cy.get(".signup__title-form");
export const signUpSuccessPageSubtitle = () => cy.get(".signup__subtitle-form");
export const termsLink = () => cy.get("[data-testid='mr-link-terms-1']");
export const privacyPolicyLink = () => cy.get("[data-testid='mr-link-privacy-1']");
export const signUpWithGoogleBtn = () => cy.get("#kmq-google-button");
export const slackBtn = () => cy.get("#kmq-slack-button");
export const officeBtn = () => cy.get("#kmq-office365-button");
export const appleBtn = () => cy.get("#apple-auth");
export const facebookBtn = () => cy.get("[data-soc='facebook']");
export const socialSitesLoginPopupTitle = () => cy.get("#socialtos-title");