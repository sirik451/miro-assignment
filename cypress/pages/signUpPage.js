import {
  name,
  email,
  password,
  termsAndConditions,
  subscriptionCheckbox,
  submitBtn,
  nameError,
  emailError,
  passwordError,
  termsError,
  shortPassword,
  weakPassword,
  sosoPassword,
  goodPassword,
  signUpSuccessPageTitle,
  signUpSuccessPageSubtitle,
  termsLink,
  privacyPolicyLink,
  signUpWithGoogleBtn,
  slackBtn,
  officeBtn,
  appleBtn,
  facebookBtn,
  socialSitesLoginPopupTitle
} from "./signUpPageLocators";

export class SignUpPage {
  navigateToSignUpPage() {
    cy.visit("https://miro.com/signup/");
  }

  setName(userName) {
    name().type(userName);
  }

  setEmail(userEmail) {
    email().type(userEmail);
  }

  setPassword(userPassword) {
    password().type(userPassword);
  }

  checkTermsAndConditionsCheckbox() {
    termsAndConditions().click();
  }

  checkSubscriptionCheckbox() {
    subscriptionCheckbox().click();
  }

  clickGetStartedNowBtn() {
    submitBtn().click();
  }

  fillSignUpForm(userData) {
    this.setName(userData.name);
    this.setEmail(userData.email);
    this.setPassword(userData.password);
    if (userData.checkTermsAndConditionsCheckbox == true) {
      this.checkTermsAndConditionsCheckbox();
    }
    if (userData.checkSubscriptionCheckbox == true) {
      this.checkSubscriptionCheckbox();
    }
  }

  getNameError() {
    return nameError();
  }

  getEmailError() {
    return emailError();
  }

  getPasswordError() {
    return passwordError();
  }

  getShortPasswordError() {
    return shortPassword();
  }

  getWeakPasswordError() {
    return weakPassword();
  }

  getSoSoPasswordError() {
    return sosoPassword();
  }

  getGoodPasswordMessage() {
    return goodPassword();
  }

  getTermsAndConditionsError() {
    return termsError();
  }

  getSignUpSuccessPageTitle() {
    return signUpSuccessPageTitle();
  }

  getSignUpSuccessPageSubtitle() {
    return signUpSuccessPageSubtitle();
  }

  clickTermLink() {
    termsLink().should($a => {
        expect($a.attr('href'), 'href').to.equal('/legal/terms-of-service/')
        expect($a.attr('target'), 'target').to.equal('_blank')
        $a.attr('target', '_self')
      }).click();
  }

  clickPrivacyPolicyLink() {
    privacyPolicyLink().should($a => {
        expect($a.attr('href'), 'href').to.equal('/legal/privacy-policy/')
        expect($a.attr('target'), 'target').to.equal('_blank')
        $a.attr('target', '_self')
      }).click();
  }

  clickSignUpWithGoogleBtn() {
    signUpWithGoogleBtn().click();
  }

  clickSlackBtn() {
    slackBtn().click();
  }

  clickOfficeBtn() {
    officeBtn().click();
  }

  clickAppleBtn() {
    appleBtn().click();
  }

  clickFacebookBtn() {
    facebookBtn().click();
  }

  getSocialSitesLoginPageTitle() {
    return socialSitesLoginPopupTitle();
  }
}
export const signUpPage = new SignUpPage();
